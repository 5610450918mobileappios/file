//
//  ViewController.swift
//  FileSystem
//
//  Created by iOS Dev on 10/13/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    
    @IBAction func clearButtonTouch(sender: AnyObject) {
        self.textField.text = ""
    }
    
    @IBAction func saveToDocument(sender:AnyObject) {
        let fm = NSFileManager()
        
        do{
            // Get url for user document directory
            let docurl = try fm.URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true)
            
            let myfolder = docurl.URLByAppendingPathComponent("MyFolder")
            
            try fm.createDirectoryAtURL(myfolder, withIntermediateDirectories: true, attributes: nil)
            
            let myFile = myfolder.URLByAppendingPathComponent("field2.txt")
            
            try self.textField.text?.writeToURL(myFile, atomically: true, encoding: NSUTF8StringEncoding)
            
            print(myFile)
            
            
        }catch{
                // Error handling here
            print(123)
        }
    }
    
    
    @IBAction func loadToDocument(sender:AnyObject) {
        let fm = NSFileManager()
        do{
            let docurl = try fm.URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: false)
        print(0)
            let myfolder = docurl.URLByAppendingPathComponent("MyFolder")
        
            let myFile = myfolder.URLByAppendingPathComponent("field2.txt")
        print(myFile)
            let fieldText = try String(contentsOfURL: myFile, encoding: NSUTF8StringEncoding)
        
        
            self.textField.text = fieldText
            
        }catch{
                // Error handling here
            print(2)
        }
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

